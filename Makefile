TOP_SIM = verilog/sim/tb.v
TOP = verilog/src/semafor.v

ISE_DS_DIR = '/opt/Xilinx/14.2/ISE_DS/'
IGNORE := $(shell bash -c "source $(ISE_DS_DIR)settings32.sh; env | sed 's/=/:=/' | sed 's/^/export /' > makeenv")                         
include makeenv   
XST = xst
NGDBUILD = ngdbuild
MAP = map
PAR = par
BITGEN = bitgen
PARTNAME = xc6slx16-csg324
BASENAME := $(notdir $(basename $(TOP)))
SIM_BASE := $(notdir $(basename $(TOP_SIM)))
SRC_V = $(wildcard verilog/src/*.v)
SIM_V = $(wildcard verilog/sim/*.v)
SDB_FILES = $(addprefix isim/src/,$(notdir $(SRC_V:.v=.sdb))) $(addprefix isim/work/,$(notdir $(SIM_V:.v=.sdb)))

all: $(BASENAME).bit

checksyntax: $(BASENAME).ngc

design.v: $(SRC_V)
	@for i in $(SRC_V); do  echo "\`include \"$$i\"" >> design.v;  done                         

$(BASENAME).ucf:
	cp board.ucf $(BASENAME).ucf

# Verilog compile
%.ngc: design.v $(SRC_V)
	echo "run -ifn design.v -ifmt Verilog -ofn $@ -ofmt NGC -p $(PARTNAME)  -opt_mode Speed -opt_level 1 -ent $(BASENAME) "  -top ${BASENAME} > xst.scr
	$(XST) -ifn xst.scr
	rm xst.scr

# Build Xilinx project DB
%.ngd: %.ngc  %.ucf
	$(NGDBUILD) -aul -p $(PARTNAME) -nt timestamp -uc $(BASENAME).ucf $< $@

# Map
%.ncd: %.ngd
	$(MAP) -p $(PARTNAME) $<

# Place and route
%-placed.ncd: %.ncd
	$(PAR)  $< $@

# Generate bitfile
%.bit: %-placed.ncd
	$(BITGEN) -b -w $< $@

isim/src/%.sdb: verilog/src/%.v 
	vlogcomp -work src $<

isim/work/%.sdb: verilog/sim/%.v 
	vlogcomp $<



sim.exe: $(SDB_FILES)
	fuse -top $(SIM_BASE)  -L src  -o sim.exe

runsim: sim.exe
	./sim

download:
	djtgcfg prog -d Nexys3 -i 0 -f $(BASENAME).bit

cleantemp:
	rm -f $(BASENAME).ngc
	rm -f $(BASENAME).ngd
	rm -f $(BASENAME).ncd
	rm -f $(BASENAME).bld
	rm -f $(BASENAME).mrp
	rm -f $(BASENAME).ngm
	rm -f $(BASENAME).pcf
	rm -f $(BASENAME).bgn
	rm -f $(BASENAME).drc
	rm -f $(BASENAME).bit
	rm -f $(BASENAME).ucf
	rm -f $(BASENAME).map
	rm -f $(BASENAME)-placed*
	rm -f $(BASENAME)_usage.xml
	rm -f *.xrpt
	rm -f *.xwbt
	rm -f $(BASENAME)_summary.xml
	rm -f *usage*.html
	rm -rf xlnx_auto*
	rm -rf isim
	rm -f sim.exe
	rm -f makeenv
	rm -f isim.wdb
	rm -f fuse*
	rm -f isim.log
	rm -f webtalk.log
	rm -rf _xmsgs
	rm -f xst.srp
	rm -f netlist.lst
	rm -rf xst
	rm -rf xst.scr
	rm -rf design.v

# clean everything
clean: cleantemp
	rm -f $(BASENAME).rbt
	rm -f $(BASENAME).rle

.PHONY: clean cleantemp
	
