module semafor( clk, dp, dpled, sa, sp, clkled);

input clk;
input dp;
output dpled;
output reg [2:0] sa;
output reg [1:0] sp;
output clkled;

parameter [1:0] S0 = 2'b00;
parameter [1:0] S1 = 2'b01;
parameter [1:0] S2 = 2'b10;
parameter [1:0] S3 = 2'b11;

parameter [2:0] SAR = 3'b001;
parameter [2:0] SAY = 3'b010;
parameter [2:0] SAYR = 3'b011;
parameter [2:0] SAG = 3'b100;
parameter [1:0] SPR = 2'b01;
parameter [1:0] SPG = 2'b10;

reg [1:0] state;
reg [1:0] next_state;

wire new_clk ;

clock5s clock_div(clk, new_clk);

assign clkled = new_clk;
assign dpled = dp;

initial begin
  state <= S0;
  next_state <= S0;
end

always @ (posedge new_clk)
begin
  state <= next_state;
end

always @(*)begin
  case(state)
    S0: 
      if(dp == 1'b1) next_state = S1;
      else next_state = S0;
    S1: 
      next_state = S2;
    S2: 
      if(dp == 1'b1) next_state = S2;
      else next_state = S3;
    S3: 
      next_state = S0;
    default:
      next_state = S0;
  endcase
end

always @(state)
begin
  case(state)
    S0: begin
      sa <= SAG;
      sp <= SPR; 
    end
    S1: begin
      sa <= SAY;
      sp <= SPR; 
    end
    S2: begin
      sa <= SAR;
      sp <= SPG; 
    end
    S3: begin
      sa <= SAYR;
      sp <= SPR; 
    end
    default: begin
      sa <= SAG;
      sp <= SPR; 
    end
  endcase
end

endmodule

//-------------------------------
module clock5s(input clk_in, output clk_out);

reg [29:0] counter = 0;
assign clk_out = counter[28];

always @(posedge clk_in)
    counter <= counter + 1;

endmodule

